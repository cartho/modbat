package modbat.test

import org.scalatest._
import modbat.mbt.ModbatTestHarness

class ObserverHarnessTest extends fixture.FlatSpec with fixture.TestDataFixture with Matchers {
  "ObserverHarnessTest1" should "pass" in {
  td =>
    ModbatTestHarness.test(Array("-s=1", "-n=2", "--no-redirect-out", "--log-level=fine", "modbat.test.ObserverHarness"), 
    (()=>ModbatTestHarness.setTestJar()), td)
  }
}
