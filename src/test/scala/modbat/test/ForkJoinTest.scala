package modbat.test                                                        
import org.scalatest._
import modbat.mbt.ModbatTestHarness
class ForkJoinTest extends fixture.FlatSpec with fixture.TestDataFixture with Matchers     {
 "ForkJoinTest1" should "pass" in {                                  
 td =>
 ModbatTestHarness.test(Array("-s=1", "-n=30", "--no-redirect-out", "--log-level=fine", "modbat.test.ForkJoin"),
 (()=>ModbatTestHarness.setTestJar()), td)
 }
}     
