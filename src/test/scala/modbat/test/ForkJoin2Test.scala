package modbat.test         
import org.scalatest._                                                   
import modbat.mbt.ModbatTestHarness
class ForkJoin2Test extends fixture.FlatSpec with fixture.TestDataFixture with Matchers {
  "ForkJoin2Test1" should "pass" in {
  td =>
  ModbatTestHarness.test(Array("-s=1", "-n=3", "--no-redirect-out", "--log-level=fine", "modbat.test.ForkJoin2"),
  (()=>ModbatTestHarness.setTestJar()), td)
 }
}

