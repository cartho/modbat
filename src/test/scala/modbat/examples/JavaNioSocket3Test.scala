package modbat.examples

import org.scalatest._
import modbat.mbt.ModbatTestHarness

class JavaNioSocket3Test extends fixture.FlatSpec with fixture.TestDataFixture with Matchers {
 "JavaNioSocket3Test1" should "pass" in {
  td =>
  ModbatTestHarness.test(Array("-n=10", "-s=1", "--no-redirect-out", "--loop-limit=5", "modbat.examples.JavaNioSocket3"),
  (() => ModbatTestHarness.setExamplesJar()), td)
 }
}

